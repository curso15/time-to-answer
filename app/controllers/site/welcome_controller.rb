class Site::WelcomeController < SiteController
  def index
    @questions = Question.includes(:answers).order('id desc').page(params[:page]).per(5)
  end
end
